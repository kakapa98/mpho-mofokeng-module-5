import 'package:flutter/material.dart';

const kPrimaryColor = Color.fromARGB(255, 118, 158, 160);
const kSecondaryColor = Color.fromARGB(242, 151, 79, 56);
const kDarkGreyColor = Color(0xFFA8A8A8);
const kWhiteColor = Color(0xFFFFFFFF);
const kZambeziColor = Color.fromARGB(255, 141, 111, 111);
const kBlackColor = Color.fromARGB(255, 102, 102, 54);
const kTextFieldColor = Color.fromARGB(255, 133, 123, 123);
const bgColor = Color.fromARGB(214, 199, 200, 201);

const kDefaultPadding = EdgeInsets.symmetric(horizontal: 30);

TextStyle titleText =
    const TextStyle(color: kPrimaryColor, fontSize: 32, fontWeight: FontWeight.w700);
TextStyle subTitle = const TextStyle(
    color: kSecondaryColor, fontSize: 18, fontWeight: FontWeight.w500);
TextStyle textButton = const TextStyle(
  color: kPrimaryColor,
  fontSize: 18,
  fontWeight: FontWeight.w700,
);