import 'package:flutter/material.dart';
import 'package:flutter_application/theme.dart';

class CheckBox extends StatefulWidget {
  // const CheckBox({Key? key}) : super(key: key);
  final String text;
  // ignore: use_key_in_widget_constructors
  const CheckBox(this.text);

  @override
  State<CheckBox> createState() => _CheckBoxState();
}

class _CheckBoxState extends State<CheckBox> {
  bool _isSelected = false;
  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        children: [
          GestureDetector(
            onTap: (() {
              setState(() {
                _isSelected = !_isSelected;
              });
            }),
            child: Container(
              width: 20,
              height: 20,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(4),
                  border: Border.all(color: kDarkGreyColor)),
            child:_isSelected ? const Icon(
              Icons.check, 
              size: 17, 
              color: 
              Colors.green,):null
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          Text(widget.text),
        ],
      )
    ]);
  }
}
