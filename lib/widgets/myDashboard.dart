// ignore: file_names
import 'package:flutter/material.dart';
import 'package:flutter_application/Screens/developer.dart';
import 'package:flutter_application/Screens/feedback.dart';
import 'package:flutter_application/Screens/mylogin.dart';
import 'package:flutter_application/Screens/user_profile.dart';

class NavDrawer extends StatelessWidget {
  const NavDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const DrawerHeader(
            decoration: BoxDecoration(
                color: Colors.green,
                image: DecorationImage(
                    fit: BoxFit.fill,
                    image: AssetImage('lib/assets/cover.png'))),
            child: Text(
              'Side menu',
              style: TextStyle(color: Colors.white, fontSize: 25),
            ),
          ),
          ListTile(
            leading: const Icon(Icons.input),
            title: const Text('App Creator'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(
              builder: (context) => const Developer(),),)},
          ),
          ListTile(
            leading: const Icon(Icons.verified_user),
            title: const Text('Profile'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(
              builder: (context) => const UserProfile(),),)},
          ),
          ListTile(
            leading: const Icon(Icons.settings),
            title: const Text('Settings'),
            onTap: () => {Navigator.of(context).pop()},
          ),
          ListTile(
            leading: const Icon(Icons.border_color),
            title: const Text('Feedback'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(builder: (context) => const FeedBack()))},
          ),
          ListTile(
            leading: const Icon(Icons.exit_to_app),
            title: const Text('Logout'),
            onTap: () => {Navigator.push(context, MaterialPageRoute(
              builder: (context) => const LogInScreen()))},
          ),
        ],
      ),
    );
  }
}
