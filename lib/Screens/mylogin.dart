import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application/Screens/dashboard.dart';
import 'package:flutter_application/Screens/mysignup.dart';
import 'package:flutter_application/theme.dart';
import 'package:flutter_application/widgets/logIn.dart';
import 'package:flutter_application/widgets/primary_button.dart';

class Splash extends StatelessWidget {
  const Splash({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
        splash: Column(
          children: [
            Image.asset('lib/assets/loading.png'),
          ],
        ),
        splashIconSize: 300,
        duration: 5000,
        backgroundColor: const Color.fromARGB(255, 71, 72, 73),
        nextScreen: const Dashboard()
      );
  }
}

class LogInScreen extends StatelessWidget {
  const LogInScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color.fromARGB(138, 110, 100, 80),
      body: Padding(
        padding: kDefaultPadding,
        child: Column(
          children: [
            const SizedBox(
              height: 120,
            ),
            Text(
              'KAKAPA',
              style: titleText,
            ),
            const SizedBox(
              height: 5,
            ),
            const SizedBox(
              height: 10,
            ),
            logIn(),
            const SizedBox(
              height: 20,
            ),
            const Text(
              'Forgot password?',
              style: TextStyle(
                color: kZambeziColor,
                fontSize: 14,
                decoration: TextDecoration.underline,
                decorationThickness: 1,
              ),
            ),
            const SizedBox(
              height: 20,
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const Splash(),
                  ),
                );
              },
              child: const PrimaryButton(
                buttonText: 'Log In',
              ),
            ),
            Row(
              children: [
                const Text(
                  'New to app?  ',
                  style: TextStyle(
                    color: Color.fromARGB(242, 174, 177, 182),
                  ),
                ),
                const SizedBox(
                  width: 5,
                ),
                GestureDetector(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => const SignUpScreen(),
                      ),
                    );
                  },
                  child: Text(
                    'Create Account',
                    style: textButton.copyWith(
                      decoration: TextDecoration.underline,
                      decorationThickness: 1,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
